#INSTALL ANSIBLE
rpm -ivh libyaml-0.1.3-4.el6_6.x86_64.rpm
rpm -ivh PyYAML-3.10-3.1.el6.x86_64.rpm
rpm -ivh python-babel-0.9.4-5.1.el6.noarch.rpm
rpm -ivh python-crypto-2.0.1-22.el6.x86_64.rpm
rpm -ivh python-crypto2.6-2.6.1-2.el6.x86_64.rpm
rpm -ivh python-httplib2-0.7.7-1.el6.noarch.rpm
rpm -ivh python-jinja2-2.2.1-2.el6_5.x86_64.rpm
rpm -ivh python-pyasn1-0.0.12a-1.el6.noarch.rpm
rpm -ivh python-keyczar-0.71c-1.el6.noarch.rpm
rpm -ivh python-paramiko-1.7.5-2.1.el6.noarch.rpm
rpm -ivh python-setuptools-0.6.10-3.el6.noarch.rpm
rpm -ivh python-simplejson-2.0.9-3.1.el6.x86_64.rpm
rpm -ivh sshpass-1.05-1.el6.x86_64.rpm
rpm -ivh ansible-1.9.4-1.el6.noarch.rpm
